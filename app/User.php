<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function get_short_created_at()
  {
        $date = $this->get_attribute('created_at');
        if (is_string($date)){
            $dateObject = DateTime::createFromFormat('Y-m-d H:i:s', $date);
            return $dateObject->format('d-m-y');
        }
        return $date;
    }
}
