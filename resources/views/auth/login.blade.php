<!DOCTYPE html>
<html lang="en">
<head>
 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <link href="{{ asset('css/login.css') }}" rel="stylesheet" /> 
<title>Kudo Login</title>
</head>
<body>
<div id="Container">
    <div id="mainBox">
      <img src="/images/logokudo.png"></img>
        <p>Discover Awesome Things</p>
            <div id="formBG">
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="uname" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="User name"><hr>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password"><hr>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                                <button id="masuk" type="submit"></button>
                                <button id="help" href=""></button>
                    </form>
                </div>
            </div>
</body>
</html>