<!DOCTYPE html>
<html lang="en">
<head>
 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <link href="{{ asset('css/home.css') }}" rel="stylesheet" />
 
<title>Dashboard</title>
</head>
<body>
<div id="Container">
<div id="box2">
  <div id="mainBox">
  
   <p><b>Welcome</b></p>
   <h1><?php echo e(Auth::user()->name); ?></h1>
   <img src="/images/agen.png" id="agen"></img>
   <img src="/images/blank_ava.png" id="avatar"><?php echo e(Auth::user()->avatar); ?></img>
    <form>
     <h2><?php echo e(Auth::user()->name); ?></h2>
     <h3>Join Date Here: <?php echo e(Auth::user()->created_at->format('d-m-y')); ?></h3>
     <h4>Descriptions</h4>
     <h5><?php echo e(Auth::user()->Description); ?></h5>
     </form>
    </div>
   <button id="keluar" a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
   document.getElementById('logout-form').submit();"></button>
   <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
   <?php echo e(csrf_field()); ?>
    </form>
  </div>
  </div>
 </div>
</body>
</html>