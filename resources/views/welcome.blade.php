<!DOCTYPE html>
<html lang="en">
<head>
 <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <link href="{{ asset('css/login.css') }}" rel="stylesheet" />
 <style>
    #uname
   {
    border: none;
    font-family:Roboto;
    margin-top:35px;
    font-size:16px;
    background: url(images/username.png) no-repeat scroll 1px 2px;
    padding-left:30px;
    width:240px;
    img:58px;
   }
   #password
   {
    border: none;
    font-family:Roboto;
    margin-top:25px;
    font-size:16px;
    background: url(images/password.png) no-repeat scroll 1px 1px;
    padding-left:30px;
    width:240px;
    img:58px;                       
   }
 </style>
<title>Kudo Login</title>
</head>
<body>
<div id="Container">
    <div id="mainBox">
    <a href="https://kudo.co.id/shop/online"><img src="/images/close.png" id="close" align="left"></a>
      <img src="/images/logokudo.png" id="logo"></img>
        <p>Discover Awesome Things</p>
            <div id="formBG">
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="uname" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="User name"><hr>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password"><hr>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            </div>
                                <button id="masuk" type="submit"></button>
                                <a href="https://kudo.co.id/shop/page/help"><img src="/images/help.png" id="help" align="right"></a>
                    </form>
                </div>
            </div>
</body>
</html>